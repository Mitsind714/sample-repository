'use strict';
const express = require('express');

const path = require('path');
const boom = require('express-boom');
const createError = require('http-errors');
const bodyParser = require('body-parser');
//const expressFileUpload = require('express-fileupload');
const router = require('./src/routes');
const app=express();
//app.use(expressFileUpload());
app.use(bodyParser.urlencoded({
    limit: '10mb',
    extended: true
}));
app.use(bodyParser.json({
  limit: '10mb',
    extended: true
}));
app.use(boom());
app.use(router());

app.use(function (req, res, next) {
    next(createError(404));
  });
  
  app.use(function (err, req, res, next) {
    console.error(err);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'dev' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });

  
module.exports = app;
var express = require('express');
var router = express.Router();
//var addDocumentService=require('./documents/addDocumentService');
const sampleRouter = require('./sample');
//const documentRouter=require('./documents/index');
const routes = () => {
    console.log("entering into routes in route.js");
    router.route('/hello').post(async (req, res) => {
        var username = req.body.username;
        var response = {
            body: JSON.stringify(username)
        }
        console.log('Otuput....' + JSON.stringify(response));
        res.send(response);
    });
    router.post('/sample',sampleRouter.sampleHello);
    //router.use('/sample',sampleRouter());
   // router.use('/api/document',documentRouter());
    return router;
}
module.exports = routes;